#ifndef BFS_H_
#define BFS_H_

#include <map>

#include "graph.h"

void bfs(
    Graph &g,
    const std::string &start,
    std::map<Vertex*, int> &dist,
    std::map<Vertex*, Vertex*> &prev);

#endif // BFS_H_

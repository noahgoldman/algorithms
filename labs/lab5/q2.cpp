#include <iostream>
#include <map>
#include <fstream>
#include <queue>
#include <stack>

#include "graph.h"
#include "bfs.h"

bool processRequest(std::istream &in, Graph &g)
{
    std::string startName;
    std::string destName;

    std::cout << "Enter start node: ";
    if( !( in >> startName ) )
        return false;
    std::cout << "Enter destination node: ";
    if (!(in >> destName))
      return false;

    std::map<Vertex*, int> dist;
    std::map<Vertex*, Vertex*> prev;
    bfs(g, startName, dist, prev);

    std::map<std::string, Vertex*>::iterator it = g.vertexMap.find(destName);
    Vertex *end_vertex = it->second;


    std::stack<Vertex*> s;
    Vertex *v = end_vertex;

    if (prev.find(v) == prev.end()) {
      std::cout << "Failed to find a path" << std::endl;
      return true;
    }

    s.push(v);
    while (prev.find(v) != prev.end()) {
      s.push(prev[v]);
      v = prev[v];
    }
    
    while (!s.empty()) {
      std::cout << s.top()->name << " ";
      s.pop();
    }
    std::cout << std::endl;

    return true;
}

//
// A simple main that reads the file given by argv[1]
// and then calls processRequest to compute shortest paths.
// Skimpy error checking in order to concentrate on the basics.
//
int main( int argc, char *argv[ ] )
{
    Graph g;

    if( argc != 2 )
    {
        std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
        return 1;
    }

    std::ifstream inFile(argv[1]);
    if( !inFile )
    {
        std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
        return 1;
    }

    std::string source, dest;

    // Read the vertices; add them to vertexMap
    while( inFile >> source >> dest )
    {
        g.addEdge( source, dest );
    }

    std::cout << "File read" << std::endl;
    while( processRequest( std::cin, g ) )
        ;

    return 0;
}

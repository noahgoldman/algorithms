#include <stdlib.h>
#include <iostream>

int gcd(int a, int b)
{
  if (b == 0) return a;
  return gcd(b, a % b);
}

int q2(int a, int b)
{
  return (a * b) / gcd(a, b);
}

int main(int argc, char *argv[])
{
  int i1 = atoi(argv[1]);
  int i2 = atoi(argv[2]);

  std::cout << q2(i1, i2) << std::endl;

  return 0;
}

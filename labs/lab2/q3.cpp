#include <stdlib.h>
#include <assert.h>
#include <iostream>

template <typename T> void swap(T *arr, int a, int b)
{
  if (a != b) {
    T temp = arr[a];
    arr[a] = arr[b];
    arr[b] = temp;
  }
}

void move_center(int *arr, int n, int k, int p)
{
  while (p < n && k < n) {
    swap(arr, k++, p++);
  }
}

void split(int *arr, const int n, const int v)
{
  int p = n, i = 0, k = 0;
  while (i < p) {
    if (arr[i] < v) {
      swap(arr, i++, k++);
    } else if (arr[i] == v) {
      swap(arr, i, --p);
    } else {
      i++;
    }
  }

  move_center(arr, n, p, k);
}

int main(int argc, char *argv[])
{
  int n = argc - 2;
  int v = atoi(argv[1]);
  int *arr = new int[n]();

  for (int i = 0, j = 2; j < argc; i++, j++) {
    arr[i] = atoi(argv[j]);
  }

  split(arr, n, v); 

  for (int j = 0; j < n; j++) {
    std::cout << arr[j] << std::endl;
  }

  return 0;
}

#include <math.h>
#include <stdlib.h>
#include <iostream>

void fill_array(bool val, bool* arr, int n)
{
  for (int i = 0; i < n; i++) {
    arr[i] = val;
  }
}

int compute_primes(int max, int* primes)
{
  bool *arr = new bool[max];
  fill_array(true, arr, max);

  for (int n = 2; n < (int) sqrt(max); n++) {
    if (arr[n]) {
      for (int i = pow(n, 2); i < max; i += n) {
        if (i >= 3) {
          arr[i] = false;
        }
      }
    }
  }

  int j = 0;
  for (int i = 3; i < max; i++) {
    if (arr[i]) {
      primes[j] = i;
      j++;
    }
  }

  delete arr;
  return j;
}

int main(int argc, char *argv[])
{
  int max = atoi(argv[1]);

  int *primes = new int[max];
  int num = compute_primes(max, primes);

  std::cout << "There are " << num << " primes" << std::endl;

  for (int i = 0; i < num; i++) {
    std::cout << primes[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}

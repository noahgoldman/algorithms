#include <iostream>
#include <algorithm>

void init_array(int **x, int len)
{
  for (int i = 0; i < len; i++) {
    for (int j = 0; j < len; j++) {
      x[i][j] = 0;
    }
  }
}

int palindrome(const std::string &str)
{
  int len = str.length();

  int x[len][len];

  for (int i = 0; i < len; i++) {
    x[i][i] = 1;
  }

  for (int st = 2; st <= len; st++) {
    for (int i = 0; i <= len - st; i++) {
      int j = i + st - 1; 
      if (str[i] == str[j]) {
        if (j - i == 1) {
          x[i][j] = 2;
        } else {
          x[i][j] = 2 + x[i + 1][j -1];
        }
      } else {
        x[i][j] = std::max(x[i + 1][j], x[i][j - 1]);
      }
    }
  }

  return x[0][len - 1];
}

int main()
{
  std::string str;

  std::cout << "Enter the string to be examined:";
  std::cin >> str;

  std::cout << "The longest palindrome has length " << palindrome(str) << std::endl;
}

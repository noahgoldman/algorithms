#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <random>

#include "graph.h"

bool in_vertex(Vertex *v, std::vector<Vertex*> &cover)
{
  return std::find(cover.begin(), cover.end(), v) != cover.end();
}

void vector_remove(Vertex *v, std::vector<Vertex*> &vector)
{
  std::vector<Vertex*>::iterator itr = std::find(vector.begin(), vector.end(), v);
  if (itr != vector.end()) {
    vector.erase(itr);
  }
}

int vertex_cover(Vertex *v)
{
  unsigned int sum_yes = 0;
  for (unsigned int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];
    vector_remove(v, u->adj);
    sum_yes += vertex_cover(u);
  }
  
  sum_yes++;

  unsigned sum_no = 0;
  sum_no += v->adj.size();
  for (unsigned int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];
    for (unsigned int j = 0; j < u->adj.size(); j++) {
      if (u->adj[j] != v) {
        sum_no += vertex_cover(u->adj[j]);
      }
    }
  }

  return std::min(sum_yes, sum_no);
}

int main(int argc, char *argv[])
{
  Graph g;

  if( argc != 2 ) {
      std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
      return 1;
  }

  std::ifstream inFile(argv[1]);
  if( !inFile )
  {
      std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
      return 1;
  }

  std::string source, dest;

  // Read the vertices; add them to vertexMap
  while( inFile >> source >> dest )
  {
      g.addEdge( source, dest );
  }

  std::random_device rd;
  std::mt19937 eng(rd());  
  std::uniform_int_distribution<> distr(0,g.allVertices.size() - 1);

  std::cout << "The smallest vertex cover has size " << vertex_cover(g.allVertices[distr(eng)]) << std::endl;  
}

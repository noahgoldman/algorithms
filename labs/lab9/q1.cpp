#include <iostream>
#include <vector>
#include <algorithm>

int edit_distance(const std::string &x, const std::string &y)
{
  int E[y.length()][x.length()];

  for (unsigned int i = 0; i < x.length(); i++) {
    E[0][i] = i;
  }

  for (unsigned int j = 0; j < y.length(); j++) {
    E[j][0] = j;
  }

  for (unsigned int i = 1; i < y.length(); i++) {
    for (unsigned int j = 1; j < x.length(); j++) {
      int t1 = E[i - 1][j] + 1;
      int t2 = E[i][j - 1] + 1;
      int t3 = E[i - 1][j - 1] + (y[i] == x[j] ? 0 : 1);

      int other_min = std::min(t1, t2);
      E[i][j] = std::min(other_min, t3);
    }
  }

  return E[y.length() - 1][x.length() - 1];
}

int main()
{
  std::string x, y;

  std::cout << "Enter the first string: ";
  std::cin >> x;

  std::cout << "Enter the second string: ";
  std::cin >> y;

  std::cout << "The edit distance is " << edit_distance(x, y) << std::endl;
}

#include <iostream>
#include <cmath>

inline double reverse_pow(double n, int e)
{
  double total = n;
  for (int i = 0; i < e; i++) {
    total *= (1/n);
  }

  return total;
}

double my_sqrt(int n, int p)
{
  double i;
  double factor = reverse_pow(10, p);
  for (i = 0; i*i < n; i += factor) {}

  return i - factor;
}

int main(int argc, char *argv[])
{
  unsigned long int n, p;
  std::cout << "Enter the number to computer the square root of: ";
  std::cin >> n;

  std::cout << "Enter the decimal place to go to: ";
  std::cin >> p;

  std::cout << "Sqrt: " << my_sqrt(n, p) << std::endl;

  std::cout << "Sqrt from cmath: " << sqrt(n) << std::endl;
}

#include <iostream>
#include <vector>
#include <algorithm>

typedef std::vector<std::pair<int, int> > Denom;

bool sort_denoms(const std::pair<int, int> &p1, const std::pair<int, int> &p2)
{
  return p1.first > p2.first;
}

void get_change(int n, const Denom &denoms)
{
  Denom output = denoms;
  int total = 0;
  for (unsigned int i = 0; i < denoms.size(); i++) {
    output[i].second = 0;
    for (int j = 0; j < denoms[i].second; j++) {
      if (total + denoms[i].first == n) {
        output[i].second++;
        std::cout << std::endl << "Results:" << std::endl;
        for (unsigned int i = 0; i < denoms.size(); i++) {
          std::cout << output[i].first << ": " << output[i].second << std::endl;
        }
        return;
      } else if (total + denoms[i].first < n) {
        output[i].second++;
        total += denoms[i].first;
      }
    }
  }
}

int main(int argc, char *argv[])
{

  Denom denoms;

  std::string in;
  std::cout << "Normal coins (n) or arbitrary (a): " << std::endl;
  std::cin >> in;

  int n;
  std::cout << "How much change: ";
  std::cin >> n;

  if (in == "n") {
    int quarters, dimes, nickels, pennies;

    std::cout << "How many quarters: ";
    std::cin >> quarters;

    std::cout << "How many dimes: ";
    std::cin >> dimes;

    std::cout << "How many nickels: ";
    std::cin >> nickels;

    std::cout << "How many pennies: ";
    std::cin >> pennies;

    denoms.push_back(std::make_pair(25, quarters));
    denoms.push_back(std::make_pair(10, dimes));
    denoms.push_back(std::make_pair(5, nickels));
    denoms.push_back(std::make_pair(1, pennies));

    std::sort(denoms.begin(), denoms.end(), sort_denoms);

    get_change(n, denoms);
  } else {
    bool another = true;
    while (another) {
      int amount, num;
      std::string res;

      std::cout << "Enter the coin amount: ";
      std::cin >> amount;

      std::cout << "Enter the number of this coin: ";
      std::cin >> num;

      denoms.push_back(std::make_pair(amount, num));

      std::cout << "Another (y/n): ";
      std::cin >> res;

      another = res == "y";
    }

    std::sort(denoms.begin(), denoms.end(), sort_denoms);

    get_change(n, denoms);
  }
}

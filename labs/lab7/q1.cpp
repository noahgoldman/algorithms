#include <iostream>
#include <algorithm>

inline int abs(int x)
{
  return x < 0 ? x * -1 : x;
}

int m2(int n, int x, float factor)
{
  std::cout << factor << std::endl;
  int res = x*x;
  float new_factor = factor / 2;
  int change = x * factor;

  if (res == n) {
    return x;
  } else if (res < n) {
    return m2(n, x + change, new_factor);
  } else { // res > n
    return m2(n, x - change, new_factor);
  }
}

int m1(int n)
{
  int i;
  for (i = 0; i*i < n; i++) {
  }

  return i - 1;
}

int main(int argc, char *argv[])
{
  unsigned long int n;
  std::cout << "Enter the number to computer the square root of: ";
  std::cin >> n;

  std::cout << "Method 1: " << m1(n) << std::endl;
  std::cout << "Method 2: " << m2(n, n / 2, 0.5) << std::endl;
}

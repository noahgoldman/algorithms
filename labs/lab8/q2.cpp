#include <cstdlib>
#include <iostream>

std::string extend_str(const std::string &str, int x) {
  std::string output;

  while (output.size() < x / 2) {
    output += str;
  }

  return output;
}

void q2(int x, int y, std::string &str) {
  int k = 0;

  std::string key = extend_str(str, x);

  for (int i = 0; i < y; i++) {
    int j = 0;
    k = 0;

    while ((i <= y /2 && k < i) || (i > (y / 2) && k < (y - i - 1))) {
      std::cout << key[k++];
      j++;
    }
    while (j < (x - k - 1)) {
      std::cout << key[k];
      j++;
    }
    while (j < x) {
      std::cout << key[k--];
      j++;
    }
    std::cout << std::endl;
  }
}

int main(int argc, char *argv[]) {
  int x, y;
  std::string str;

  std::cout << "Enter x: ";
  std::cin >> x;

  std::cout << "Enter y: ";
  std::cin >> y;

  std::cout << "Enter the string to use: ";
  std::cin >> str;

  q2(x, y, str);
}

#include <vector>
#include <iostream>
#include <map>
#include <fstream>
#include <queue>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <cstring>

std::ifstream::pos_type filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::in | std::ifstream::binary);
    in.seekg(0, std::ifstream::end);
    return in.tellg(); 
}

class CharNode {
  public:
    CharNode(std::string chr, int freq);
    CharNode(std::string str);

    std::string ToString() const;

    inline bool IsLeaf() const {
      return this->left == NULL && this->right == NULL;
    }

    CharNode *parent;
    CharNode *left;
    CharNode *right;

    std::string val;
    int freq;
};

CharNode::CharNode(std::string val, int freq) : val(val), freq(freq) {
  this->parent = NULL;
  this->left = NULL;
  this->right = NULL;
}

CharNode::CharNode(std::string str) {
  char c_val[100];
  int i_freq = 0;
  sscanf(str.c_str(), "(%s ,%d)", c_val, &i_freq);

  std::string actual_val(c_val);

  if (actual_val == "~") {
    actual_val = "";
  }

  this->val = actual_val;
  this->freq = i_freq;

  this->parent = NULL;
  this->left = NULL;
  this->right = NULL;
}

std::string CharNode::ToString() const {
  std::string id = this->val.empty() ? "~" : this->val;

  char *str = new char[100];
  sprintf(str, "(%s ,%d)", id.c_str(), this->freq);
  return std::string(str);
}

struct OrderCharNode {
  bool operator() (const CharNode *c1, const CharNode *c2) { 
    return c1->freq > c2->freq;
  }
};

std::string read_file(const char *filename)
{
  std::ifstream file(filename);
  std::string output;
  
  if (!file) {
    std::cerr << "Failed to open " << filename << std::endl;
    std::exit(EXIT_FAILURE);
  }

  file.seekg(0, std::ios::end);
  output.resize(file.tellg());
  file.seekg(0, std::ios::beg);
  file.read(&output[0], output.size());
  file.close();

  return output;
}

void get_frequencies(const char *filename, std::map<std::string, int> &chars)
{
  std::ifstream file(filename);
  std::string token;
  
  while (file >> token) {
    ++chars[token];
  }
}

CharNode* huffman(std::vector<CharNode*> nodes)
{
  std::priority_queue<CharNode*, std::vector<CharNode*>, OrderCharNode> queue(nodes.begin(), nodes.end());

  while (queue.size() > 1) {
    CharNode *i = queue.top();
    queue.pop();

    CharNode *j = queue.top();
    queue.pop();

    int sum = i->freq + j->freq;

    CharNode *k = new CharNode("", sum);

    if (i->freq < j->freq) {
      k->left = i;
      k->right = j;
    } else {
      k->right = i;
      k->left = j;
    }

    i->parent = k;
    j->parent = k;

    nodes.push_back(k);
    queue.push(k);
  }

  assert(!queue.empty());
  return queue.top();
}

void convert_tree_array(CharNode *node, std::vector<CharNode*> &nodes, int i) {
  if (node == NULL) {
    return;
  }

  unsigned int max_size = 2*i + 2;
  if (nodes.size() < max_size) {
    nodes.resize(max_size);
  }

  nodes[i] = node;

  convert_tree_array(node->left, nodes, 2*i + 1);
  convert_tree_array(node->right, nodes, 2*i + 2);
}

std::string vector_serialize(std::vector<CharNode*> &nodes) {
  std::string output;
  for (unsigned int i = 0; i < nodes.size(); i++) {
    if (nodes[i] == NULL) {
      output += "~";
    } else {
      output += nodes[i]->ToString();
    }
    output += "|";
  }

  return output;
}

void serialize_tree(CharNode *node, std::ostream &out) {
  if (node == NULL) {
    out << "# ";
  } else {
    out << node->val << " ";
    serialize_tree(node->left, out);
    serialize_tree(node->right, out);
  }  
}

void unserialize_tree(std::vector<CharNode*> &nodes) {
  for (unsigned int i = 0; i < nodes.size(); i++) {
    if (nodes[i] != NULL) {
      if (i != 0) {
        nodes[i]->parent = nodes[(i-1)/2];
      }

      nodes[i]->left = nodes[2*i + 1];
      nodes[i]->right = nodes[2*i + 2];
    }
  }
}

std::string encode_str(CharNode *node, const std::string &str) {
  if (node->left == NULL) {
    return "";
  } else if (node->left->val == str) {
    return "0";
  } else if (node->right->val == str) {
    return "1";
  } 

  std::string out = encode_str(node->left, str);
  if (!out.empty()) {
    return "0" + out;
  }

  out = encode_str(node->right, str);
  if (!out.empty()) {
    return "1" + out;
  }

  return "";
}

void encode_file(const char *filename, CharNode *root, std::ostream &out)
{
  std::ifstream in(filename);
  std::string token;

  while (in >> token) {
    out << encode_str(root, token).c_str() << " ";
  }
  out << std::endl;
}

std::string decode(std::string str, CharNode *root)
{
  std::string output;
  unsigned int i = 0;

  std::stringstream stream(str);
  std::string token;

  while (stream >> token) {
    CharNode *node = root;
    i = 0;
    while (!node->IsLeaf()) {
      assert((node->left == NULL && node->right == NULL) || (node->left != NULL && node->right != NULL));
      assert(token[i] == '0' || token[i] == '1');
      if (token[i] == '0') {
        node = node->left;
      } else {
        node = node->right;
      }
      i++;
    }

    output += node->val;
    output += " ";
  }

  return output;
}

void decode_file(const char *filename)
{
  std::ifstream in(filename);
  if (!in) {
    std::cerr << "failed to open file" << std::endl;
  }

  FILE *f = fopen(filename, "r");
  char *buf = new char[1000000000];
  std::string tree;
  while (tree[tree.size() - 2] != '&') {
    fgets(buf, 10000000, f);
    tree += std::string(buf);
  }

  std::string wat, part;
  while (wat[wat.size() - 1] != '&') {
    std::getline(in, part);
    wat += part;
  }

  std::vector<CharNode*> nodes;

  char *tree_str = new char[tree.size() + 1];
  strcpy(tree_str, tree.c_str());

  char *p = strtok(tree_str, "|");
  while (strstr(p, "&") != p) {
    std::string token(p);
    if (token == "~") {
      nodes.push_back(NULL);
    } else {
      CharNode *node = new CharNode(token);
      nodes.push_back(node);
    }

    p = strtok(NULL, "|");
  }

  unserialize_tree(nodes);

  CharNode *root = nodes[0];

  std::string coded;
  std::string token;
  while (in >> token) {
    coded += token;
    coded += " ";
  }

  std::cout << decode(coded, root);
}

char* get_output_filename(const char *filename) {
  char *new_file = new char[strlen(filename) + 5];
  std::string ext = ".huf";

  strcpy(new_file, filename);
  strcat(new_file, ext.c_str());

  return new_file;
}

void q1(const char *filename)
{
  std::map<std::string, int> chars;

  get_frequencies(filename, chars);

  std::vector<CharNode*> nodes;

  for (std::map<std::string, int>::const_iterator itr = chars.begin();
     itr != chars.end();
     itr++) {
    CharNode *node = new CharNode(itr->first, itr->second);
    nodes.push_back(node);
  }

  CharNode *root = huffman(nodes);

  std::vector<CharNode*> nodeTree;
  convert_tree_array(root, nodeTree, 0);

  char *outfile = get_output_filename(filename);

  std::ofstream out;
  out.open(outfile);

  out << vector_serialize(nodeTree);
  out << "&";
  out << std::endl;

  encode_file(filename, root, out);

  std::cout << "Created " << outfile << std::endl;

  int size_original = filesize(filename);
  int size_after = filesize(outfile);

  float percent = ((float)size_after / (float)size_original) * 100;

  std::cout << "(compressed " << size_original << " bytes to " << 
    size_after << " bytes [" << percent << "% of original size]" << std::endl;
}

int main(int argc, char *argv[])
{
  if (argc == 1) {
    std::cerr << "Needs two arguments" << std::endl; 
  }

  char *filename = argv[1];

  if (argc > 2) {
    decode_file(filename);
  } else {
    q1(filename);
  }

}

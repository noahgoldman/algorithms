#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <vector>
#include <fstream>

#define SIZE 5

std::unordered_set<char*> words;
std::unordered_map<char, std::vector<char*> > buckets;

void print_grid(char grid[][SIZE])
{
    for (int i = 0; i < SIZE; ++i) {
        printf("%.5s\n", grid[i]);
    }
}

char* get_str(const std::string &word)
{
    char *str = new char[word.size()];
    std::copy(word.begin(), word.end(), str);
    return str;
}

bool add_row(char grid[][SIZE], char *word, int row)
{
    for (int i = 0; i < SIZE; i++) {
        grid[row][i] = word[i];
    }

    return true;
}

bool add_column(char grid[][SIZE], char *word, int column)
{
    for (int i = 0; i < SIZE; i++) {
        grid[i][column] = word[i];
    }

    return true;
}

inline bool word_exists(char *word)
{
    return words.find(word) != words.end();
}

bool is_solution(char grid[][SIZE])
{
    for (int i = 0; i < SIZE; i++) {
        char word[5];
        for (int j = 0; j < SIZE; j++) {
            word[j] = grid[i][j];
        }
        
        if (!word_exists(word)) {
            return false;
        }
    }

    return true;
}

void build_buckets()
{
    for (std::unordered_set<char*>::const_iterator itr = words.begin();
            itr != words.end();
            itr++) {
        buckets[(*itr)[0]].push_back(*itr);
    }
}

void try_column(char grid[][SIZE], int column)
{
    int next = column + 1;
    std::vector<char*> bucket = buckets[grid[0][column]];
    for (unsigned int i = 0; i < bucket.size(); ++i) {
        add_column(grid, bucket[i], column);
        if (next < SIZE) {
            try_column(grid, next);
            //std::cout << is_solution(grid) << std::endl;
        } else {
        }
    }
}

void try_word(char *word)
{
    char grid[SIZE][SIZE] = { ' ' };
    std::cout << word << std::endl;

    add_row(grid, word, 0);

    try_column(grid, 0);
}

void read_file(std::ifstream &file)
{
    std::string word;
    while (file >> word) {
        char *str = get_str(word);
        words.insert(str);
    }
}

int main(int argc, char *argv[])
{
    std::ifstream file(argv[1]);
    if (!file) {
        std::cerr << argv[1] << std::endl;
        std::cerr << "Failed to read words from file" << std::endl;
        exit(1);
    }

    read_file(file);
    build_buckets();

    for (std::unordered_set<char*>::const_iterator itr = words.begin();
            itr != words.end();
            ++itr) {
        try_word(*itr); 
    } 
}

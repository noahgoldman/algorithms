#ifndef Q1_H_
#define Q1_H_

#include <vector>
#include <list>
#include <string>
#include <unordered_map>

#include "graph.h"

void dfs(Graph &g, Vertex *v, std::vector<std::vector<Vertex*> > &paths);

void explore(Vertex*v, std::unordered_map<Vertex*, bool> visited,
                            const std::string &end, std::unordered_map<Vertex*, Vertex*> prev);

Graph* reverse(const Graph &g);

void print_path(std::vector<Vertex*> &path);

#endif // Q1_H_

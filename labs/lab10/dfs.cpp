#include <iostream>
#include <map>
#include <fstream>
#include <list>

#include "graph.h"
#include "dfs.h"

void previsit(Vertex *v, int &count, int &cc)
{
  v->pre = count;
  v->cc = cc;
  ++count;
}

void postvisit(Vertex *v, int &count)
{
  v->post = count;
  ++count;
}

void print_path(std::vector<Vertex*> &path)
{
  for (unsigned int i = 0; i < path.size(); i++) {
    Vertex *v = path[i];
    std::cout << v->name << " ";
  }
  std::cout << std::endl;
}

void get_path(std::unordered_map<Vertex*, Vertex*> &prev,
    Vertex *end,
    std::vector<Vertex*> &path)
{
  Vertex *v = prev[end];

  path.push_back(v);
  while (v != end) {
    path.push_back(prev[v]);
    v = prev[v];
  }
}

void explore(
    Vertex *v,
    std::unordered_map<Vertex*, bool> visited,
    Vertex *end,
    std::unordered_map<Vertex*, Vertex*> prev,
    int vertices_left,
    std::vector<std::vector<Vertex*> > &paths)
{
  if (v == end && visited[v]) {
      if (vertices_left == 0) {
          std::vector<Vertex*> path;
          get_path(prev, end, path);
          paths.push_back(path); 
      }
      return;
  }

  visited[v] = true;

  for (unsigned int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];

    if (!visited[u] || u == end) {
        prev[u] = v;    
        explore(u, visited, end, prev, vertices_left - 1, paths);
    }
  }
}

void dfs(
  Graph &g,
  Vertex *start,
  std::vector<std::vector<Vertex*> > &paths)
{
  std::unordered_map<Vertex*, bool> visited;

  std::unordered_map<Vertex*, Vertex*> prev;
  prev[start] = NULL;

  explore(start, visited, start, prev, g.allVertices.size(), paths);
}

Graph* reverse(const Graph &g)
{
  Graph *gr = new Graph();

  for (std::vector<Vertex*>::const_iterator i = g.allVertices.begin(); i != g.allVertices.end(); i++) {
    Vertex *v = *i;
    for (std::vector<Vertex*>::const_iterator j = v->adj.begin(); j != v->adj.end(); j++) {
      Vertex *u = *j;
      gr->addEdge(u->name, v->name);
    }
  }

  return gr;
}

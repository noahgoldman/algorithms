#include <vector>
#include <map>
#include <fstream>
#include <algorithm>

#include "graph.h"
#include "dfs.h"

typedef std::pair<Vertex*, Vertex*> Edge;
std::map<Edge, int> weights;

int get_cost(const std::vector<Vertex*> path)
{
    int cost = 0;
    for (unsigned int i = 0; i < path.size() - 1; i++) {
        Edge edge = std::make_pair(path[i], path[i+1]);
        cost += weights[edge];
    }

    Edge cycle_edge = std::make_pair(path[0], path[path.size() - 1]);
    cost += weights[cycle_edge];

    return cost;
}

Vertex* get_vertex(Graph &g, const std::string &name)
{
    for (unsigned int i = 0; i < g.allVertices.size(); i++) {
        if (g.allVertices[i]->name == name) {
            return g.allVertices[i];
        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{    
    Graph g;

    if( argc != 2 )
    {
        std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
        return 1;
    }

    std::ifstream inFile(argv[1]);
    if( !inFile )
    {
        std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
        return 1;
    }

    std::string source, dest;
    int weight;

    // Read the vertices; add them to vertexMap
    while(inFile >> source >> dest >> weight)
    {
        g.addEdge( source, dest );
        Vertex *s = get_vertex(g, source);
        Vertex *d = get_vertex(g, dest);
        Edge edge = std::make_pair(s, d);
        weights[edge] = weight;
    }

    std::vector<std::vector<Vertex*> > paths;
    dfs(g, g.allVertices[0], paths); 

    std::vector<Vertex*> min_path;
    int min_cost = INT_MAX;
    for (unsigned int i = 0; i < paths.size(); i++) {
        int cost = get_cost(paths[i]);
        if (cost < min_cost) {
            min_cost = cost;
            min_path = paths[i];
        }
    }

    std::cout << "Solution:" << std::endl;
    print_path(min_path);
    std::cout << "Cost: " << min_cost << std::endl;
}

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "graph.h"
#include "bfs.h"

typedef std::map<std::string, std::vector<std::string> > Buckets;

template <class T> 
bool map_compare(const T &m1, const T &m2)
{
  return m1.second < m2.second;
}

template <class T>
typename T::const_iterator get_max_element(const T &map)
{
  return std::max_element(map.begin(),
      map.end(),
      map_compare<typename T::value_type>);
}

void add_vertex_bucket(const std::string &name, Buckets &buckets)
{
  std::string str;
  for (unsigned int i = 0; i < name.length(); i++) {
    str = name;
    str[i] = '_';
    buckets[str].push_back(name);
  }
}

void add_edges(const Buckets &buckets, Graph &g)
{
  for (Buckets::const_iterator itr = buckets.begin(); itr != buckets.end(); itr++) {
    std::vector<std::string> array = itr->second;
    for (unsigned int i = 0; i < array.size(); i++) {
      for (unsigned int j = 0; j < array.size(); j++) {
        if (i != j) {
          g.addEdge(array[i], array[j]);
        }
      }
    }
  }
}

void get_path(Graph &g,
    std::map<Vertex*, Vertex*> &prev,
    const std::string &end_name,
    std::vector<Vertex*> &path)
{
  Vertex *end = g.getVertex(end_name);
  Vertex *v = end;

  path.push_back(end);
  while (prev.find(v) != prev.end()) {
    path.push_back(prev[v]);
    v = prev[v];
  }
}

int main( int argc, char *argv[ ] )
{
  Graph g;
  Buckets buckets;

  std::string line;
  std::ifstream inFile(argv[1]);
  if(inFile.is_open()) {
    while (getline(inFile, line)) {
      add_vertex_bucket(line, buckets);
    }
  }
  inFile.close();

  add_edges(buckets, g);

  std::vector<Vertex*> max_path;
  int max_length = 0;

  for (unsigned int i = 0; i < g.allVertices.size(); i++) {
    std::map<Vertex*, int> dist;
    std::map<Vertex*, Vertex*> prev;
    bfs(g, g.allVertices[i]->name, dist, prev);

    std::map<Vertex*, int>::const_iterator max = get_max_element(dist);
    
    if (max->second > max_length) {
      max_length = max->second;
      get_path(g, prev, max->first->name, max_path); 
      std::cout << max_length << std::endl;
    }
  }
      
  std::cout << "The longest path with length " << max_length << " is:" << std::endl;
  for (int i = max_path.size() - 1; i > 0; i--) {
    std::cout << max_path[i]->name << " => ";
  }
  std::cout << max_path[0]->name << std::endl;
}

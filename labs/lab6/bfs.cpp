#include <iostream>
#include <queue>

#include "bfs.h"

bool check_exists(std::map<Vertex*, int> &map, Vertex *v)
{
  return map.find(v) != map.end();
}

void bfs(
    Graph &g,
    const std::string &start,
    std::map<Vertex*, int> &dist,
    std::map<Vertex*, Vertex*> &prev)
{
  std::map<std::string, Vertex*>::iterator it = g.vertexMap.find(start);
  Vertex *start_vertex = it->second;

  dist[start_vertex] = 0;

  std::queue<Vertex*> q;
  q.push(start_vertex);

  while (!q.empty()) {
    Vertex *v = q.front();
    q.pop();
    for (unsigned int i = 0; i < v->adj.size(); i++) {
      Vertex *u = v->adj[i];
      if (!check_exists(dist, u)) {
        q.push(u);
        dist[u] = dist[v] + 1;
        prev[u] = v;
#ifdef L5_Q1
        std::cout << "Vertex " << u->name << " has distance " << dist[u] << std::endl;
#endif
      }
    }
  }
}

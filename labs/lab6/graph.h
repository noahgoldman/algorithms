#ifndef GRAPH_H_
#define GRAPH_H_

#include <iostream>
#include <map>
#include <vector>
#include <climits>

#define INFINITY INT_MAX

class Vertex {
  public:
    std::string           name;   // Vertex name
    std::vector<Vertex *> adj;    // Adjacent vertices
    int              dist;   // Cost
    Vertex          *path;   // Previous vertex on shortest path

    // pre and post
    int pre;
    int post;
    int cc;

    Vertex( const std::string & nm ) : name( nm )
      { reset( ); }

    void reset( )
      { dist = INFINITY; path = NULL; pre = 0; post = 0; cc = 0; }
};

typedef std::map<std::string,Vertex *> vmap;
typedef std::pair<std::string,Vertex *> vpair;

class Graph
{
  public:
    Graph( ) { }
    ~Graph( );
    void addEdge( const std::string & sourceName, const std::string & destName );
    void printPath( const std::string & destName ) const;
    void unweighted( const std::string & startName );

    vmap vertexMap;
    std::vector<Vertex *> allVertices;
    Vertex * getVertex( const std::string & vertexName );
      
  private:
    void printPath( const Vertex & dest ) const;
    void clearAll( );

};

#endif // GRAPH_H_

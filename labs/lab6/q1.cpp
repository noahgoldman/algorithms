#include <iostream>
#include <fstream>
#include <vector>

#include "graph.h"
#include "bfs.h"

typedef std::map<std::string, std::vector<std::string> > Buckets;

void add_vertex_bucket(const std::string &name, Buckets &buckets)
{
  std::string str;
  for (unsigned int i = 0; i < name.length(); i++) {
    str = name;
    str[i] = '_';
    buckets[str].push_back(name);
  }
}

void add_edges(const Buckets &buckets, Graph &g)
{
  for (Buckets::const_iterator itr = buckets.begin(); itr != buckets.end(); itr++) {
    std::vector<std::string> array = itr->second;
    for (unsigned int i = 0; i < array.size(); i++) {
      for (unsigned int j = 0; j < array.size(); j++) {
        if (i != j) {
          g.addEdge(array[i], array[j]);
        }
      }
    }
  }
}

void get_shortest_path(Graph &g,
    std::map<Vertex*, Vertex*> &prev,
    const std::string &end_name,
    std::vector<Vertex*> &path)
{
  Vertex *end = g.getVertex(end_name);
  Vertex *v = end;

  path.push_back(end);
  while (prev.find(v) != prev.end()) {
    path.push_back(prev[v]);
    v = prev[v];
  }
}

int main( int argc, char *argv[ ] )
{
  Graph g;
  Buckets buckets;

  std::string line;
  std::ifstream inFile(argv[1]);
  if(inFile.is_open()) {
    while (getline(inFile, line)) {
      add_vertex_bucket(line, buckets);
    }
  }
  inFile.close();

  add_edges(buckets, g);

  std::string startName;
  std::string destName;

  std::cout << "Enter start word: ";
  if (!(std::cin >> startName)) return false;
  std::cout << "Enter destination word: ";
  if (!(std::cin >> destName)) return false;
  std::cout << std::endl;

  std::map<Vertex*, int> dist;
  std::map<Vertex*, Vertex*> prev;
  bfs(g, startName, dist, prev);

  std::vector<Vertex*> path;
  get_shortest_path(g, prev, destName, path);
    
  for (int i = path.size() - 1; i > 0; i--) {
    std::cout << path[i]->name << " => ";
  }
  std::cout << path[0]->name << std::endl;
}

#include <fstream>
#include <iostream>
#include <vector>

#include "graph.h"

void print_edges(Graph &g)
{
  for (unsigned int i = 0; i < g.allVertices.size(); i++) {
    Vertex *v = g.allVertices[i];
    for (unsigned int j = 0; j < v->adj.size(); j++) {
      Vertex *u = v->adj[j];
      std::cout << v->name << "->" << u->name << std::endl;
    }
  }
}

void q2(Graph &g)
{
  Graph gr;

  for (std::vector<Vertex*>::iterator i = g.allVertices.begin(); i != g.allVertices.end(); i++) {
    Vertex *v = *i;
    for (std::vector<Vertex*>::iterator j = v->adj.begin(); j != v->adj.end(); j++) {
      Vertex *u = *j;
      gr.addEdge(u->name, v->name);
    }
  }

  print_edges(gr);
}

//
// A simple main that reads the file given by argv[1]
// and then calls processRequest to compute shortest paths.
// Skimpy error checking in order to concentrate on the basics.
//
int main( int argc, char *argv[ ] )
{
    Graph g;

    if( argc != 2 )
    {
        std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
        return 1;
    }

    std::ifstream inFile(argv[1]);
    if( !inFile )
    {
        std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
        return 1;
    }

    std::string source, dest;

    // Read the vertices; add them to vertexMap
    while( inFile >> source >> dest )
    {
        g.addEdge( source, dest );
    }

    q2(g);

    return 0;
}

#include <iostream>
#include <map>
#include <fstream>

#include "graph.h"

std::string explore(
    Vertex *v,
    std::map<Vertex*, bool> &visited,
    const std::string &end)
{
  visited[v] = true;

  if (v->name == end) {
    return v->name;
  }

  for (int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];

    if (!visited[u]) {
      std::string out = explore(u, visited, end);
      if (!out.empty()) {
        return v->name + " " + out;
      }
    }
  }

  return "";
}

const std::string dfs(
  Graph &g,
  const std::string &start,
  const std::string &end)
{
  std::map<std::string, Vertex*>::iterator it = g.vertexMap.find(start);
  
  vpair start_vertex = *it;
  std::map<Vertex*, bool> visited;

  return explore(start_vertex.second, visited, end);
}

bool processRequest(std::istream &in, Graph &g)
{
    std::string startName;
    std::string destName;

    std::cout << "Enter start node: ";
    if( !( in >> startName ) )
        return false;
    std::cout << "Enter destination node: ";
    if( !( in >> destName ) )
        return false;

    std::string output = dfs(g, startName, destName);
    std::cout << output << std::endl;

    return true;
}

//
// A simple main that reads the file given by argv[1]
// and then calls processRequest to compute shortest paths.
// Skimpy error checking in order to concentrate on the basics.
//
int main( int argc, char *argv[ ] )
{
    Graph g;

    if( argc != 2 )
    {
        std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
        return 1;
    }

    std::ifstream inFile(argv[1]);
    if( !inFile )
    {
        std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
        return 1;
    }

    std::string source, dest;

    // Read the vertices; add them to vertexMap
    while( inFile >> source >> dest )
    {
        g.addEdge( source, dest );
    }

    std::cout << "File read" << std::endl;
    while( processRequest( std::cin, g ) )
        ;

    return 0;
}

#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <cassert>

void get_center(const std::vector<int> &nums, int &negative, int &positive)
{
  int last = nums[0];
  negative = 0;

  for (int i = 0; i < nums.size(); i++) {
    if (nums[i] != 0) {
      if (nums[i] > 0) {
        positive = i;
        return;
      }

      negative = i;
    }
  }

  assert(0);
}

bool q2(std::vector<int> &nums)
{
  std::sort(nums.begin(), nums.end());
  
  int positive, negative;
  get_center(nums, positive, negative);
  
  while (negative >= 0 && positive < nums.size()) {
    if ((nums[negative] + nums[positive]) == 0) {
      return true;
    } else if (abs(nums[negative]) < abs(nums[positive])) {
      negative--;
    } else {
      positive++;
    }
  }
}

int main(int argc, char *argv[])
{
  std::vector<int> arr;
  
  for (int i = 1; i < argc; i++)
  {
    arr.push_back(atoi(argv[i]));
  }

  if (q2(arr)) {
    std::cout << "There are two elements in this's array whose sum is zero"
      << std::endl;
  } else {
    std::cout << "No elements who sum to zero" << std::endl;
  }

  return 0;
}

#include <iostream>
#include <cstdlib>
#include <stdio.h>

inline int square(int n)
{
  return n * n;
}

int find_root_iter(int n)
{
  int x = n / 2;
  int last_diff = n / 2;

  while (!(square(x) == n)) {
    int half = last_diff / 2;
    int diff = half == 0 ? 1 : half;

    if (square(x) > n) {
      x -= diff;
    } else {
      x += diff;
    }

    last_diff = diff;
  }

  return x;
}

int q3(int n)
{
  return find_root_iter(n);
}

int main(int argc, char *argv[])
{
  int num = atoi(argv[1]);

  std::cout << q3(num) << std::endl;
}

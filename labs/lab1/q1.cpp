#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

// Get the expected sum of all the integers from 1 to n.
inline int64_t sum_of_ints(int64_t n)
{
  return (n * (n + 1)) / 2;
}

// Get the expected sum of all the squares of the integers from 1 to n.
inline int64_t sum_of_squares(int64_t n)
{
  return (n * (n + 1) * (2*n + 1))/6;
}

inline int64_t get_square(int64_t n)
{
  return n * n;
}

int64_t sum_ints(int64_t *nums, int64_t n, bool square = false)
{
  int64_t len = n - 2; // Two numbers are missing.
  int64_t sum = 0;

  for (int64_t i = 0; i < len; i++)
  {
    int64_t num;
    if (square) {
      num = get_square(nums[i]);
    } else {
      num = nums[i];
    }

    sum += num;
  }

  return sum;
}

int64_t sum_squares(int64_t *nums, int64_t n)
{
  return sum_ints(nums, n, true);
}

inline int64_t get_sum_diff(int64_t *nums, int64_t n)
{
  return sum_of_ints(n) - sum_ints(nums, n);
}

inline int64_t get_squares_diff(int64_t *nums, int64_t n)
{
  return sum_of_squares(n) - sum_squares(nums, n);
}

inline int64_t compute_num(int64_t sum_diff, int64_t squares_diff)
{
  return (0.5 * (sqrt(2 * squares_diff - get_square(sum_diff)) + sum_diff));
}

void q1(int64_t *nums, int64_t n, int64_t &i1, int64_t &i2)
{
  int64_t max = pow(2, n) - 1;
  int64_t int_diff = get_sum_diff(nums, max);
  int64_t squares_diff = get_squares_diff(nums, max);

  i1 = compute_num(int_diff, squares_diff);
  i2 = int_diff - i1;
}

//---------------------------------------------
// Functions to generate the arrays for testing
//---------------------------------------------

void get_nums(int64_t range, int64_t &i1, int64_t &i2)
{
  srand(time(NULL));
  i1 = (rand() % range) + 1;
  i2 = (rand() % range) + 1;
}

int main(int argc, char *argv[])
{
  int64_t n = atoi(argv[1]);
  int64_t max = pow(2, n) - 1;

  int64_t i1, i2;
  get_nums(max, i1, i2);

  printf("The generated random numbers are %d and %d\n\n", i1, i2);

  int64_t *nums = new int64_t[max - 2];

  int64_t i = 0;
  for (int64_t num = 1; num <= max; num++)
  {
    if (num != i1 && num != i2)
    {
      nums[i] = num;
      i++;
    }
  }

  int64_t ans1, ans2;
  q1(nums, n, ans1, ans2);

  printf("The computed numbers are %d and %d\n", ans1, ans2);

  delete[] nums;

  return 0;
}

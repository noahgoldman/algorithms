#ifndef Q1_H_
#define Q1_H_

#include <vector>
#include <list>
#include <string>

#include "graph.h"

std::list<Vertex*>* dfs(Graph &g, const std::string &start, const std::string &end);

std::list<Vertex*>* explore(Vertex*v, std::map<Vertex*, bool> &visited,
                            const std::string &end, int &count, int &cc);

Graph* reverse(const Graph &g);

void print_path(std::list<Vertex*>*);

#endif // Q1_H_

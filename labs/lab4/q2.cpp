#include <fstream>
#include <algorithm>
#include <cassert>

#include "graph.h"
#include "dfs.h"

int count = 1;
int cc = 0;

void copy_pre_post(std::vector<Vertex*> &input, std::vector<Vertex*> &output)
{
  for (unsigned int i = 0; i < input.size(); i++) {
    for (unsigned int j = 0; j < output.size(); j++) {
      if (input[i]->name == output[j]->name) {
        output[j]->pre = input[i]->pre;
        output[j]->post = input[i]->post;
      }
    }
  }
}
<<<<<<< HEAD

void previsit(Vertex* v)
{
  v->pre = count;
  v->cc = cc;
  count++;
}

void postvisit(Vertex *v)
{
  v->post = count;
  count++;
}

bool sort_by_post(Vertex *v1, Vertex *v2) {
  return v1->post > v2->post;
}

void merge(std::vector<Vertex*> *to, std::vector<Vertex*> *from) {
  for (unsigned int i = 0; i < from->size(); i++) {
    to->push_back((*from)[i]);
  }
}

void explore(
    Vertex *v,
    std::map<Vertex*, bool> &visited)
{
  visited[v] = true;

  previsit(v);

  for (unsigned int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];

    if (!visited[u]) {
      explore(u, visited);    
    }
  }
    
  postvisit(v);
}

std::vector<Vertex*> my_dfs(std::vector<Vertex*> &vertices)
{
  std::map<Vertex*, bool> visited;

  count = 1;
  cc = 0;

  for (unsigned int i = 0; i < vertices.size(); i++) {
    if (!visited[vertices[i]]) {
      explore(vertices[i], visited);
      ++cc;
    }
  }

  return vertices;
}

void identify(Graph &g)
{
  Graph *gr = reverse(g);
  std::vector<Vertex*> reverse_vertices = gr->allVertices;
  std::vector<Vertex*> vertices = g.allVertices;

  my_dfs(reverse_vertices);

  copy_pre_post(reverse_vertices, vertices);

  std::sort(vertices.begin(), vertices.end(), sort_by_post);

  my_dfs(vertices);  

  for (int i = 0; i < cc; i++) {
    std::cout << "SCC: ";
    for (unsigned int j = 0; j < vertices.size(); j++) {
      Vertex *v = vertices[j];
      if (v->cc == i) {
        std::cout << v->name << " ";
      }
    }
    std::cout << std::endl;
  }
=======

bool compare_post_nums(Vertex* v1, Vertex* v2)
{
  return v1->post > v2->post;
}

void print_cc(std::list<Vertex*> *vertices)
{
  int max_cc = 0;
  for (std::list<Vertex*>::iterator i = vertices->begin(); i != vertices->end(); i++) {
    max_cc = std::max(max_cc, (*i)->cc);
  }

  for (int i = 0; i <= max_cc; i++) {
    for (std::list<Vertex*>::iterator j = vertices->begin(); j != vertices->end(); j++) {
      if ((*j)->cc == i) {
        std::cout << (*j)->name << std::endl;
      }
    }
    std::cout << std::endl << std::endl;
  }
}

std::list<Vertex*>* full_dfs(std::vector<Vertex*> &vertices)
{
  bool first = true;
  std::list<Vertex*> *result;
  std::map<Vertex*, bool> visited;
  std::string null_str;

  int count = 1;
  int cc = 0;

  for (unsigned int i = 0; i < vertices.size(); i++, cc++) {
    Vertex *v = vertices[i];
    if (!visited[v]) {
      std::list<Vertex*> *out = explore(v, visited, null_str, count, cc);
      if (out != NULL) {
        if (first) {
          result = out;
          first = false;
        } else {
          result->splice(result->end(), *out);
        }
      }
    }
  }

  assert(!first);
  return result;
}

void identify(Graph &g)
{
  Graph *gr = reverse(g);

  std::string start, end;
  std::vector<Vertex*> vertices = gr->allVertices;

  std::list<Vertex*> *res = full_dfs(vertices);
  std::vector<Vertex*> result(res->begin(), res->end());

  std::sort(result.begin(), result.end(), compare_post_nums);

  print_path(res);

  for (int i = 0; i < result.size(); i++) {
    std::cout << result[i]->name << std::endl;
  }

  copy_pre_post(result, vertices);
  std::sort(vertices.begin(), vertices.end(), compare_post_nums);

  std::list<Vertex*> *last = full_dfs(vertices);

  print_path(last);

  print_cc(last);
}

//
// A simple main that reads the file given by argv[1]
// and then calls processRequest to compute shortest paths.
// Skimpy error checking in order to concentrate on the basics.
//
int main( int argc, char *argv[ ] )
{
    Graph g;

    if( argc != 2 )
    {
        std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
        return 1;
    }

    std::ifstream inFile(argv[1]);
    if( !inFile )
    {
        std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
        return 1;
    }

    std::string source, dest;

    // Read the vertices; add them to vertexMap
    while( inFile >> source >> dest )
    {
        g.addEdge( source, dest );
    }

    identify(g);

    return 0;
}

#include <iostream>
#include <map>
#include <fstream>
#include <list>
#include <algorithm>

#include "graph.h"

int count = 0;

void previsit(Vertex* v)
{
  v->pre = count;
  count++;
}

void postvisit(Vertex *v)
{
  v->post = count;
  count++;
}

bool sort_by_post(Vertex *v1, Vertex *v2) {
  return v1->pre < v2->pre;
}

void merge(std::vector<Vertex*> *to, std::vector<Vertex*> *from) {
  for (unsigned int i = 0; i < from->size(); i++) {
    to->push_back((*from)[i]);
  }
}

bool explore(
    Vertex *v,
    std::map<Vertex*, bool> &visited)
{
  visited[v] = true;

  previsit(v);

  for (unsigned int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];

    if (!visited[u]) {
      if (explore(u, visited)) {
        postvisit(v);
        return true;
      }
    }
  }
    
  postvisit(v);
  return false;
}

std::vector<Vertex*> my_dfs(std::vector<Vertex*> &vertices)
{
  std::map<Vertex*, bool> visited;

  count = 1;

  for (unsigned int i = 0; i < vertices.size(); i++) {
    if (!visited[vertices[i]]) {
      explore(vertices[i], visited);
    }
  }

  return vertices;
}

bool processRequest(std::istream &in, Graph &g)
{
    std::string startName;
    std::string destName;

    std::cout << "Enter start node: ";
    if( !( in >> startName ) )
        return false;
    std::cout << "Enter destination node: ";
    if( !( in >> destName ) )
        return false;

    std::vector<Vertex*> vertices = my_dfs(g.allVertices);
    std::sort(vertices.begin(), vertices.end(), sort_by_post);

    for (unsigned int i = 0; i < vertices.size(); i++) {
      Vertex *v = vertices[i];
      std::cout << v->name << " [" << v->pre << "," << v->post << "]" << std::endl;
      if (v->name == destName) {
        break;
      }
    }

    return true;
}

//
// A simple main that reads the file given by argv[1]
// and then calls processRequest to compute shortest paths.
// Skimpy error checking in order to concentrate on the basics.
//
int main( int argc, char *argv[ ] )
{
    Graph g;

    if( argc != 2 )
    {
        std::cerr << "Usage: " << argv[ 0 ] << " graphfile" << std::endl;
        return 1;
    }

    std::ifstream inFile(argv[1]);
    if( !inFile )
    {
        std::cerr << "Cannot open " << argv[ 1 ] << std::endl;
        return 1;
    }

    std::string source, dest;

    // Read the vertices; add them to vertexMap
    while( inFile >> source >> dest )
    {
        g.addEdge( source, dest );
    }

    std::cout << "File read" << std::endl;
    while( processRequest( std::cin, g ) )
        ;

    return 0;
}

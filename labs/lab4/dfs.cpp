#include <iostream>
#include <map>
#include <fstream>
#include <list>

#include "graph.h"
#include "dfs.h"

void previsit(Vertex *v, int &count, int &cc)
{
  v->pre = count;
  v->cc = cc;
  ++count;
}

void postvisit(Vertex *v, int &count)
{
  v->post = count;
  ++count;
}

void print_path(std::list<Vertex*> *path)
{
  if (path == NULL) {
    std::cerr << "Failed to find a path" << std::endl;
    return;
  }

  for (std::list<Vertex*>::iterator i = path->begin(); i != path->end(); i++) {
    Vertex *v = *i;
    std::cout << v->name << " [" << v->pre << "," << v->post << "]" << std::endl;
  }
  std::cout << std::endl;
}

std::list<Vertex*>* explore(
    Vertex *v,
    std::map<Vertex*, bool> &visited,
    const std::string &end,
    int &count,
    int &cc)
{
  std::list<Vertex*> *out = new std::list<Vertex*>();
  visited[v] = true;

  previsit(v, count, cc);

  if (v->name == end) {
    postvisit(v, count);
    out->push_front(v);
    return out;
  }

  for (unsigned int i = 0; i < v->adj.size(); i++) {
    Vertex *u = v->adj[i];

    if (!visited[u]) {
      std::list<Vertex*> *out = explore(u, visited, end, count, cc);
      if (out != NULL && !out->empty()) {
        postvisit(v, count);
        out->push_front(v);
        return out;
      }
    }
  }

  postvisit(v, count);
  if (end.empty()) {
    std::list<Vertex*> *out = new std::list<Vertex*>();
    out->push_front(v);
    return out;
  }
  return NULL;
}

std::list<Vertex*>* dfs(
  Graph &g,
  const std::string &start,
  const std::string &end)
{
  std::map<std::string, Vertex*>::iterator it = g.vertexMap.find(start);
  
  vpair start_vertex = *it;
  std::map<Vertex*, bool> visited;

  int count = 1;
  int cc = 0;

  std::list<Vertex*>* out = explore(start_vertex.second, visited, end, count, cc);

  return out;
}

Graph* reverse(const Graph &g)
{
  Graph *gr = new Graph();

  for (std::vector<Vertex*>::const_iterator i = g.allVertices.begin(); i != g.allVertices.end(); i++) {
    Vertex *v = *i;
    for (std::vector<Vertex*>::const_iterator j = v->adj.begin(); j != v->adj.end(); j++) {
      Vertex *u = *j;
      gr->addEdge(u->name, v->name);
    }
  }

  return gr;
}
